FROM amazoncorretto:8

RUN yum install -y postgresql-jdbc
RUN yum install -y tar
RUN yum install -y gzip
RUN echo "JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")" | tee -a   /etc/profile | source /etc/profile
RUN curl https://ftp.kddi-research.jp/infosystems/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz | tar zxv
RUN mkdir /opt/maven | mv apache-maven-3.6.3 /opt/maven/
RUN ln -s /opt/maven/apache-maven-3.6.3 /opt/maven/default
RUN echo 'export PATH=$PATH:/opt/maven/default/bin' | tee -a /etc/profile | source /etc/profile
RUN mkdir /home/work

COPY ./jdbc-1.0-SNAPSHOT.jar /home/work/

CMD ["java","-jar","/home/work/jdbc-1.0-SNAPSHOT.jar"]